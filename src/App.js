import logo from './logo.svg';
import './App.css';
import Counter from './counter/Counter';
// import Animals from './animals';
// import image from "./img/pic.jpg";

function App() {

  return (
    <div className="app-content">
      <Counter />
    {/* <Animals name="Dog" color="white" /> */}
    {/* <img src={image} /> */}
    </div>
  );
}

export default App;
